package org.niwla23.vertretungsplan

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*



class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(this /* Activity context */)
        var server = sharedPreferences.getString("server", "")
        var schoolclass = sharedPreferences.getString("class", "")
        var darkmode = sharedPreferences.getBoolean("darkmode_toggle", false)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webview1.settings.javaScriptEnabled = true
        WebView.setWebContentsDebuggingEnabled(true)
        webview1.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                swipe_container.isRefreshing = false
            }
        }

        var url = "file:///android_asset/frontend/index.html?server=" + server + "&class=" + schoolclass
        if (darkmode == false) {
            url = url + "&light"
        }

        webview1.loadUrl(url)

        swipe_container.setOnRefreshListener {
            server = sharedPreferences.getString("server", "")
            schoolclass = sharedPreferences.getString("class", "")
            url = "file:///android_asset/frontend/index.html?server=" + server + "&class=" + schoolclass
            if (darkmode == false) {
                url = url + "&light"
            }
            webview1.loadUrl(url)
        }

        fab.setOnClickListener { view ->
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings ->
                true
            else ->
                super.onOptionsItemSelected(item)
        }
    }

}

